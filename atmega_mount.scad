include <project_constants.scad>

$fn=90;

t = atmega_t;

module l(dim,h)
{
  translate([0,0,dim[2]/2]) union()
  {
    translate([-dim[0]/2 - dim[2]/2,0,h/2-dim[2]/2]) cube([dim[2],dim[1],h], center=true);
    cube([dim[0],dim[1],dim[2]], center=true);
  }
}

module a(dim,r,h)
{
  difference()
  {
    l(dim,h);
    w = 3;
    translate([-5-dim[2],-atmega_lashes_width/2,atmega_lashes_zoffset])
      cube([dim[2],atmega_lashes_width,atmega_lashes_height]);
    cylinder(dim[2],r=r);
  }
}

module a1()
{
  a([atmega_foot_lenght,atmega_foot_width,t],2,rpi_height);
}

module l1()
{
  l([atmega_foot_lenght,atmega_front_offset,t],rpi_height);
}

atmega_holes_translate = [12,9,0];

module front()
{
    translate([5+t,11,0])
    {
      a1();
      translate([0,-8,0]) l1();
      
      translate([base_holes[1][0],0,0]) rotate([0,0,180]) a1();
      translate([base_holes[1][0],-8,0]) rotate([0,0,180]) l1();
    }
    translate([0,0,rpi_height]) difference()
    {
      cube([base_holes[1][0]+2*5+2*t,22,t]);
      translate(atmega_holes_translate)
      {
        translate(atmega_holes[0]) cylinder(3,r=2);
        translate(atmega_holes[1]) cylinder(3,r=2);
      }
    }
}

module backIntern()
{
    translate([5+t,11,0])
    {
      translate(base_holes[2]) a1();
      translate(base_holes[3]) rotate([0,0,180]) a1();
    }
    translate([0,0,rpi_height]) difference()
    {
      translate([0,6,0]) translate(base_holes[2])
        cube([base_holes[1][0]+2*5+2*t,10,t]);
      translate(atmega_holes_translate)
      {
        translate(atmega_holes[2]) cylinder(3,r=2);
        translate(atmega_holes[3]) cylinder(3,r=2);
      }
    }
}

module back()
{
  translate([0,-86,0]) backIntern();
}

translate([0,0,0]) front();
translate([0,0,0]) backIntern();
