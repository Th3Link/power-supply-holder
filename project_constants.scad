// drawing url https://cdn.instructables.com/F7T/J2I0/I697TI4L/F7TJ2I0I697TI4L.LARGE.jpg
base_holes = [[0,0,0],[60,0,0],[0,80,0],[60,80,0]];

atmega_holes_x =  53.3-2*2.54;
atmega_holes_y1 = 82.55;
atmega_holes_y2 = 1.3;
atmega_holes_y3 = 50.8+24.1;
atmega_holes_y4 = atmega_holes_y1 - atmega_holes_y2 - atmega_holes_y3;
atmega_holes = [
  [0,0,0],
  [atmega_holes_x,atmega_holes_y4,0],
  [0,atmega_holes_y1,0],
  [atmega_holes_x,atmega_holes_y4+atmega_holes_y3,0]
];
atmega_lashes_yoffset = 20;
atmega_lashes_zoffset = 5;
atmega_lashes_width = 3;
atmega_lashes_height = 5;
atmega_lashes_d = base_holes[2][1];

atmega_t = 2;
atmega_foot_lenght = 10;
atmega_foot_width = 10;
atmega_front_offset = 6;
atmega_total_width = base_holes[1][0] + 2*atmega_foot_lenght/2 + 2*atmega_t;
atmega_total_lenght = base_holes[2][1] + 2*atmega_foot_lenght/2 + atmega_front_offset;

lid_t = 2;

rpi_holes = [[0,0,0],[50,0,0],[0,60,0],[50,60,0]];
rpi_height = 25;

lid_pcd_height = 62;
lid_fan_holes = [50,50,2.5];
lid_fan = [60,60,25];
lid = [atmega_total_width,115,lid_pcd_height + lid_fan[2] + 3];
lid_outer = [lid[0] + 2*lid_t, lid[1], lid[2] + lid_t];
