module roundEdge(radius,height)
{
  difference()
  {
    translate([0,0,0]) cube([radius,radius,height]);
    translate([radius,radius,0]) cylinder(height,r=radius);
  }
}
