use <roundEdge.scad>

$fn=90;

hole_to_bottom = 23+5;
l_top = hole_to_bottom+10;

difference()
{
  cube([5,20,l_top]);
  translate([0,10,hole_to_bottom]) rotate([0,90,0]) cylinder(7,r=2);
  translate([2,10,hole_to_bottom]) rotate([0,90,0]) cylinder(4,r=5);
  translate([0,0,l_top])  rotate([0,90,0]) roundEdge(10,7);
  translate([6,20,l_top]) rotate([0,90,180]) roundEdge(10,7);
}

l_bottom = 25;

difference()
{
cube([l_bottom,20,5]);
translate([l_bottom,0,0]) rotate([0,0,90]) roundEdge(10,7);
translate([l_bottom,20,0]) rotate([0,0,180]) roundEdge(10,7);
}
