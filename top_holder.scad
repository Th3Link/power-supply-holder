use <roundEdge.scad>
use <roundEdgeRectangle.scad>

$fn=90;
on_depth = 4;
on_height = 45;
on_width = 140;
round_radius = 10;

root_and_space_to_hole = on_height;

difference()
{
  cube([on_depth,on_width, on_height]);
  translate([0,20,0]) cube([on_depth,80,30]);
  //round edges
  translate([0,0,0]) rotate([90,0,90]) roundEdge(round_radius,on_depth);
  translate([5,20,0]) rotate([0,0,180]) rotate([90,0,90]) roundEdge(round_radius,on_depth);
  translate([0,80+20,0]) rotate([90,0,90]) roundEdge(round_radius,on_depth);
  translate([0,on_width-30,on_height-35])
  {
    translate([0,0,0]) rotate([0,90,0]) cylinder(on_depth,r=2);
    translate([0,-100,0]) rotate([0,90,0]) cylinder(on_depth,r=2);
    translate([0,0,0]) rotate([0,90,0]) cylinder(3,r=5);
    translate([0,-100,0]) rotate([0,90,0]) cylinder(3,r=5);
  }
}

translate([0,140-3,0]) difference()
{
  cube([80,3,on_height]);
  translate([20,0,0]) difference()
  {
    cube([60,3,30]);
    translate([0,5,0]) rotate([90,270,0]) roundEdge(round_radius,3);
    translate([60,5,30]) rotate([90,270,0]) roundEdge(5,3);
    translate([0,5,30]) rotate([90,90,0]) roundEdge(round_radius,3);
  }

  translate([on_depth+6,0,on_height-6-3]) rotate([270,0,0])
  {
    cylinder(3,r=2);
    translate([64,0,0]) cylinder(3,r=1.7);
  }
}

translate([0,0,on_height]) difference()
{
  l = 80;
  w=140;
  lx = 80+on_depth;
  wx = 140-3;
  cube([l,w,2]);
  translate([80,0,0]) rotate([0,0,90]) roundEdge(10,5);
  translate([25,20,0]) roundEdgeRectangle([35,45,5],r=5);
  translate([25,70,0]) roundEdgeRectangle([35,45,5],r=5);
  
  //rpi holes: 50x60
  xpi = 50;
  ypi = 60;

  translate([lx/2-xpi/2,wx/2-ypi/2,0])
  {
    cylinder(3,r=2);
    translate([xpi,0,0])  cylinder(3,r=2);
    translate([0,ypi,0])  cylinder(3,r=2);
    translate([xpi,ypi,0])  cylinder(3,r=2);
  }

  //mount atmega on top. hols for that
  xatmega = 60;
  yatmega = 80;
  translate([lx/2-xatmega/2,wx/2-yatmega/2,-1])
  {
    cylinder(3,r=2);
    translate([xatmega,0,0])  cylinder(3,r=2);
    translate([0,yatmega,0])  cylinder(3,r=2);
    translate([xatmega,yatmega,0])  cylinder(3,r=2);
  }
}
