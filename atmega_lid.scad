include <project_constants.scad>

$fn=90;

// right part
cube([lid_t,lid[1],lid[2]]);

// left part
translate([lid[0]+lid_t,0,0]) cube([lid_t,lid[1],lid[2]]);

// lid/top
translate([0,0,lid[2]]) {
  difference()
  {
    cube([lid_outer[0],lid[1],lid_t]);
    translate([(lid_outer[0]-lid_fan[0])/2, (lid_outer[1]-lid_fan[1])/2, 0]) fan_hole(lid_fan, lid_fan_holes, lid_t);
  }
}
rail_r = lid_t*1.5;
// front rail
translate([0,rail_r,0]) rail(lid_outer[2],rail_r);
translate([lid_outer[0],rail_r,0]) rotate([0,0,180]) rail(lid_outer[2],rail_r);

// middle rail
translate([0,lid_outer[1]/2,0]) rail(lid_outer[2],rail_r);
translate([lid_outer[0],lid_outer[1]/2,0]) rotate([0,0,180]) rail(lid_outer[2],rail_r);

//back rail
translate([0,lid_outer[1]-rail_r,0]) rail(lid_outer[2],rail_r);
translate([lid_outer[0],lid_outer[1]-rail_r,0]) rotate([0,0,180]) rail(lid_outer[2],rail_r);

// lashes
translate([0,atmega_lashes_yoffset,atmega_lashes_zoffset])
{
  translate([lid_t,0,0]) clip();
  translate([lid_t,atmega_lashes_d,0]) clip();
  translate([lid_t+lid[0],atmega_lashes_d,0]) mirror([1,0,0]) clip();
  translate([lid_t+lid[0],0,0]) mirror([1,0,0]) clip();
}
module clip()
{
  difference()
  {
    cube([atmega_t, atmega_lashes_width, atmega_lashes_height]);
    rotate([0,20,0]) cube([atmega_t, atmega_lashes_width, atmega_lashes_height*2]);
  }
}

module rail(h,r)
{
  difference()
  {
    cylinder(h,r=r);
    translate([0,-r,0])cube([r, 2*r,h]);
  }
}

module fan_hole(fan_dim,holes_dim,t)
{
  difference()
  {
    fr = 16;
    cube([fan_dim[0],fan_dim[1],t]);
    translate([fr,0,0]) rotate([0,0,135]) cube([fan_dim[0],fan_dim[1],t]);
    translate([fan_dim[0]-fr,0,0]) rotate([0,0,315]) cube([fan_dim[0],fan_dim[1],t]);
    translate([0,fan_dim[1]-fr,0]) rotate([0,0,45]) cube([fan_dim[0],fan_dim[1],t]);
    translate([fan_dim[0]-fr,fan_dim[1],0]) rotate([0,0,315]) cube([fan_dim[0],fan_dim[1],t]);
  }
  translate([(fan_dim[0]-holes_dim[0])/2,(fan_dim[1]-holes_dim[1])/2,0])
  {
    cylinder(t,r=holes_dim[2]);
    translate([holes_dim[0],0,0]) cylinder(t,r=holes_dim[2]);
    translate([0,holes_dim[1],0]) cylinder(t,r=holes_dim[2]);
    translate([holes_dim[0],holes_dim[1],0]) cylinder(t,r=holes_dim[2]);
  }
}
