module roundEdgeRectangle(v,r)
{
  h = v[0];
  w = v[1];
  d = v[2];
  difference()
  {
    cube([h,w,d]);
    translate([0,0,0]) rotate([0,0,0]) roundEdge(r,d);
    translate([h,0,0]) rotate([0,0,90]) roundEdge(r,d);
    translate([h,w,0]) rotate([0,0,180]) roundEdge(r,d);
    translate([0,w,0]) rotate([0,0,270]) roundEdge(r,d);
  }
}
